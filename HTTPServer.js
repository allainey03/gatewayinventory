const http = require("http");
const StaticServer = require("node-static").Server;
const file = new StaticServer('./public');
let mysql = require('mysql');
let qs = require('querystring');
const conn = mysql.createConnection({ host: "localhost", user : "testdb", password : "testdb", database : "testdb" });
let handlers = {};
conn.connect();

handlers["/signin"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			let account = JSON.parse(data);
			findFromDB("users", account, (rslt) => {
				res.writeHead(200, {'Content-Type' : 'application/json'});
				res.end(JSON.stringify(rslt));
			});
		});
	}
};

handlers["/inventory"] = (req, res) => {
	if(!req.url) {
		file.serveFile("/inventory.html", 200, {}, req, res);
	}
	else {
		understandRemainingPath(res, req.url, "inventory_view");
	}
};

handlers["/deliveries"] = (req, res) => {
	if(req.url && req.url.startsWith("/add")) {
		file.serveFile("/delivery_add.html", 200, {}, req, res);
	}
	else {
		file.serveFile("/delivery.html", 200, {}, req, res);
	}
};

handlers["/delivery"] = (req, res) => {
	if(req.method == "GET") {
		understandRemainingPath(res, req.url, "deliveries_view");
	}
	else if(req.method == "POST") {
		extractData(req, (data) => {
			let obj_data = JSON.parse(data);
			let insert_data = {
				supplier_id : obj_data.supplier_id,
				item_id : obj_data.item_id,
				quantity : obj_data.quantity,
				buying_price : obj_data.buying_price,
				delivery_date : obj_data.delivery_date
			};
			insertIntoDB("deliveries", insert_data, (result) => {
				if(result.result) {
					let sqlQuery = "UPDATE " + conn.escapeId("inventory") + " SET quantity = quantity + 1 WHERE ?? = ?";
					let queryString = mysql.format(sqlQuery + ";", ["item_id", obj_data.item_id]);
					conn.query(queryString, (err, results, fields) => {
						if(err) {
							console.log("DB query error: " + err);
							res.end(JSON.stringify({error: err}));
						}
						else {
							console.log("DB query results: ");
							console.log(results);
							res.end(JSON.stringify({success : "Successfully added to deliveries and updated inventory"}));
						}
					});
				}
				else if(result.error) {
					res.end(JSON.stringify({error: result.error}));
				}
			});
		});
	}
};

handlers["/item"] = (req, res) => {
	understandRemainingPath(res, req.url, "items");
};

handlers["/items_base"] = (req, res) => {
	understandRemainingPath(res, req.url, "items_base");
};

handlers["/supplier"] = (req, res) => {
	understandRemainingPath(res, req.url, "suppliers");
};

http.createServer((req, res) => {
	let url = req.url.match(/(\/[^\/\?]*)([\/\?].+)?/);
	console.log(req.url);
	let h = handlers[url[1]];
	if(h) {
		req.url = "";
		if(url[2] != null) {
			req.url = decodeURI(url[2]);
		}
		h(req, res);
	}
	else {
		file.serve(req, res, (err) => {
			res.writeHead(404, {'Content-Type': 'text/html'});
			res.end("Error 404: Page not found");
		});
	}
}).listen(8080);

console.log("Server running");

let extractData = (req, callback) => {
	let data = "";
	req.on('data', (chunk) => {
		console.log("Extracting data..");
		data += chunk;
	});
	req.on('end', () => {
		console.log("Extracted: " + data);
		callback(data);
	});
};

let findFromDB = (table, obj, /*sort, */ callback) => {
	let sqlQuery = "SELECT * FROM " + conn.escapeId(table);
	let inserts = [];
	for(let i = 0; obj && i < Object.keys(obj).length; i++) {
		sqlQuery += (i == 0) ? " WHERE ?? = ?" : " AND ?? = ?";
		inserts.push(Object.keys(obj)[i]);
		inserts.push(Object.values(obj)[i]);
	}
	let queryString = mysql.format(sqlQuery + ";", inserts);
	console.log("Query: " + queryString);
	conn.query(queryString, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error: err});
		}
		else {
			console.log("DB query results: ")
			console.log(results);
			callback({result: results});
		}
	});
};

let insertIntoDB = (table, data, callback) => {
	let sqlQuery = "INSERT INTO " + conn.escapeId(table) + " SET ?";
	conn.query(sqlQuery, data, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error: err});
		}
		else {
			console.log("DB query results:");
			console.log(results);
			callback({result: results});
		}
	});
};

let findLikeFromDB = (table, query, callback) => {
	let sqlQuery;
	let queryString;
	if(query["any"]) {
		sqlQuery = "SELECT * FROM " + conn.escapeId(table) + " WHERE supplier LIKE ? OR item LIKE ? OR brand LIKE ?";
		let values = [];
		for(let i = 0; i < 3; i++) {
			values.push("%" + Object.values(query)[0] + "%");
		}
		queryString = mysql.format(sqlQuery + ";", values);
	}
	else {
		sqlQuery = "SELECT * FROM " + conn.escapeId(table) + " WHERE ?? LIKE ?";
		queryString = mysql.format(sqlQuery + ";", [Object.keys(query)[0], "%" + Object.values(query)[0] + "%"]);
	}
	console.log("Query: " + queryString);
	conn.query(queryString, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error : err});
		}
		else {
			console.log("DB query results: ")
			console.log(results);
			callback({result : results});
		}
	});
};

let understandRemainingPath = (res, url, table) => {							//rename function something to do with path
	if(url && url.startsWith("?")) {											//with query
		let query = qs.parse(url.replace("?", ""));
		findFromDB(table, query, (rslt) => {
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else if(url && url.startsWith("/search")) {
		let query = qs.parse(url.replace("/search?", ""));
		findLikeFromDB(table, query, (rslt) => {
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
	else if(url && url.startsWith("/id")) {										//select by id
		getPath(url, (paths) => {
			if(paths.length == 2) {
				let obj = {};
				obj[paths[0]] = paths[1];
				findFromDB(table, obj, (rslt) => {
					res.writeHead(200, { "Content-Type" : "application/json" });
					res.end(JSON.stringify(rslt));
				});
			}
		});
	}
	else if(url && url.startsWith("/all")) {
		findFromDB(table, {}, (rslt) => {										//select all
			res.writeHead(200, { "Content-Type" : "application/json" });
			res.end(JSON.stringify(rslt));
		});
	}
};

/* let verifyQuery = (columns, table, callback) => {
	switch(table) {
		case "deliveries_view":
			let deliveries_columns = ["id", "supplier", "item", "brand", "description", "quantity", "buying_price", "delivery_date"];
			for (let c of columns) {
				if(!deliveries_columns.includes(c)){
					callback(false);
				}
			}
			break;
		case "inventory_view":
			let inventory_columns = ["id", "supplier", "item", "brand", "description", "quantity", "buying_price", "delivery_date"];
			break;

	}
	callback(true);
}; */

let getPath = (url, callback) => {
	let reg = /[^\/]+/g;
	let arr = [];
	while (res = reg.exec(url)) {
		arr.push(res[0]);
	}
	callback(arr);
};
