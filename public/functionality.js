let session = JSON.parse(localStorage.getItem("sessionDetails"));
let currentTab = "";
let ip = "http://localhost:8080";
/* window.onload = () => {
	checkSession();
}; */

let checkSession = (callback) => {
	if(session && session.id) {
		/* document.getElementById("cover").classList.add("hidden");
		document.getElementById("welcome_message").innerHTML = "<h2>Welcome " + session.name + "!</h2>"; */
		callback(true);
	}
	else {
		callback(false);
	}
};

let login = () => {
	session = {id : "123", name : "Allaine Bautista"};
	localStorage.setItem("sessionDetails", JSON.stringify(session));
	checkSession(() => {
		document.getElementById("cover").classList.add("hidden");
		document.getElementById("welcome_message").innerHTML = "<h2>Welcome " + session.name + "!</h2>";
	});
};

let loadInventoryTable = () => {
	getRequest("inventory/all", (rslt) => {
		let rslt_json = JSON.parse(rslt);
		let inventory_table = document.getElementById("inventory_table");
		if(rslt_json.result) {
			populateTable(inventory_table, ["id", "item", "brand", "description", "quantity", "selling_price"], rslt_json.result);
		}
		else {
			console.log("Error");
		}
	});
};

let cancelClicked = (origin) => {
	if(origin === 'cover') {
		document.getElementById("cover").className = "hidden";
	}
};

/* let searchDeliveries = () => {
	let type = document.getElementById("select_deliveries_type").value;
	let search = document.getElementById("deliveries_search").value;
	getRequest("delivery/search?" + type + "=" + search, (rslt) => {
		console.log(rslt);
		let rslt_json = JSON.parse(rslt);
		let deliveries_table = document.getElementById("deliveries_table");
		if(rslt_json.result) {
			populateTable(deliveries_table, ["id","supplier","item","brand","description","quantity","buying_price","delivery_date"], rslt_json.result);
		}
		else {
			console.log("Error");
		}
	});
}; */

let getRequest = (path, callback) => {
	let rq = new XMLHttpRequest();
	rq.open("GET", ip + "/" + path, true);
	rq.onreadystatechange = () => {
		if(rq.readyState == 4) {
			callback(rq.responseText);
		}
	};
	rq.send();
};

let postRequest = (path, data, callback) => {
	let rq = new XMLHttpRequest();
	rq.open("POST", ip + "/" + path, true);
	rq.onreadystatechange = () => {
		if(rq.readyState == 4) {
			callback(rq.responseText);
		}
	};
	rq.setRequestHeader("Content-Type", "application/json");
	rq.send(JSON.stringify(data));
};

let populateTable = (table, columns, entries) => {
	let body;
	for(let child of table.children) {
		if(child.tagName == "TBODY") {
			body = child;
			while(body.hasChildNodes()){
				body.removeChild(body.lastChild);
			}
			// body.innerHTML = "";
			break;
		}
	}
	if(body == null) {
		body = table.appendChild(document.createElement("tbody"));
	}
	for(let entry of entries) {
		let new_row = body.insertRow(-1);
		for(let c = 0; c < columns.length; c++) {
			let cell = entry[columns[c]];
			if(typeof cell == "string" && cell.match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g)) {
				let date = new Date(cell);
				cell = date.toLocaleDateString();
			}
			new_row.insertCell(c).appendChild(document.createTextNode(cell));
		}
		/* new_row.insertCell(0).appendChild(document.createTextNode(entry.id));
		new_row.insertCell(1).appendChild(document.createTextNode(entry.supplier));
		new_row.insertCell(2).appendChild(document.createTextNode(entry.item));
		new_row.insertCell(3).appendChild(document.createTextNode(entry.description));
		new_row.insertCell(4).appendChild(document.createTextNode(entry.brand));
		new_row.insertCell(5).appendChild(document.createTextNode(entry.quantity));
		new_row.insertCell(6).appendChild(document.createTextNode(entry.buying_price)); */

		/* let date = new Date(entry.delivery_date);
		date.setDate(date.getDate() - 1);
		new_row.insertCell(7).appendChild(document.createTextNode(date.toLocaleDateString())); */
	}
};
