DROP TABLE IF EXISTS `deliveries`;
DROP TABLE IF EXISTS `inventory`;
DROP TABLE IF EXISTS `transactions`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `items`;
DROP TABLE IF EXISTS `items_base`;
DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `users` (
	`username` varchar(50) NOT NULL,
	`password` varchar(255) NOT NULL,
	`first_name` varchar(50) NOT NULL,
	`last_name` varchar(50) NOT NULL,
	PRIMARY KEY (`username`)
);

INSERT INTO users (username, password, first_name, last_name) VALUES ('allainey', '12345', 'Allaine', 'Bautista');
INSERT INTO users (username, password, first_name, last_name) VALUES ('pauly', '55555', 'Paul', 'Bajo');

CREATE TABLE `items_base` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`stock_num` varchar(255) NOT NULL,
	`brand` varchar(255) NOT NULL,
	`type` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

INSERT INTO items_base (stock_num, brand, type) VALUES ('UX331UN', 'ASUS', 'laptop');
INSERT INTO items_base (stock_num, brand, type) VALUES ('Rechargeable fan', 'Tough Mama', 'fan');
INSERT INTO items_base (stock_num, brand, type) VALUES ('4K TV','Samsung', 'television');
INSERT INTO items_base (stock_num, brand, type) VALUES ('16GB flash drive', 'Lexar', 'flash drive');

CREATE TABLE `items` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`item_base_id` int(11) NOT NULL,
	`description` varchar(255) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `item_base_id` (`item_base_id`),
	CONSTRAINT `items_ibfk_1` FOREIGN KEY (`item_base_id`) REFERENCES `items_base` (`id`)
);

INSERT INTO items (item_base_id, description) VALUES (1, '13in 4GB RAM');
INSERT INTO items (item_base_id, description) VALUES (2, 'stand fan');
INSERT INTO items (item_base_id, description) VALUES (3, '32in Smart TV');
INSERT INTO items (item_base_id, description) VALUES (4, 'flash drive');

CREATE TABLE `suppliers` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`address` varchar(255) NOT NULL,
	`mobile_no` bigint(20) NOT NULL,
	PRIMARY KEY (`id`)
);

INSERT INTO suppliers (name, address, mobile_no) VALUES ('Supplier1', 'Bajada, Davao City', 09114455667);
INSERT INTO suppliers (name, address, mobile_no) VALUES ('Supplier2', 'Makati City', 09874569123);
INSERT INTO suppliers (name, address, mobile_no) VALUES ('Supplier3', 'Mati City', 09789456123);
INSERT INTO suppliers (name, address, mobile_no) VALUES ('Supplier4', 'Buhangin, Davao City', 09332156447);

CREATE TABLE `deliveries` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`supplier_id` int(11) NOT NULL,
	`item_id` int(11) NOT NULL,
	`quantity` int(11) NOT NULL,
	`buying_price` int(11) NOT NULL,
	`delivery_date` date NOT NULL,
	PRIMARY KEY (`id`),
	KEY `supplier_id` (`supplier_id`),
	KEY `item_id` (`item_id`),
	CONSTRAINT `deliveries_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`),
	CONSTRAINT `deliveries_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
);

INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (1, 2, 10, 250000, '2017-01-05');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (4, 1, 5, 6400000, '2017-12-10');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (3, 4, 20, 120000, '2018-05-17');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (2, 3, 8, 2700000, '2018-07-22');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (1, 1, 10, 250000, '2017-09-05');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (2, 2, 12, 6400000, '2017-10-10');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (3, 3, 8, 120000, '2018-08-17');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (4, 4, 15, 2700000, '2018-01-22');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (1, 3, 10, 250000, '2018-02-05');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (2, 4, 12, 6400000, '2018-11-10');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (3, 1, 8, 120000, '2018-06-17');
INSERT INTO deliveries (supplier_id, item_id, quantity, buying_price, delivery_date) VALUES (4, 2, 15, 2700000, '2018-02-22');

CREATE TABLE `inventory` (
	`item_id` int(11) NOT NULL,
	`quantity` int(11) NOT NULL,
	`selling_price` int(11) NOT NULL,
	KEY `item_id` (`item_id`),
	CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
);

INSERT INTO inventory (item_id, quantity, selling_price) VALUES (1, 23, 6000000);
INSERT INTO inventory (item_id, quantity, selling_price) VALUES (2, 37, 300000);
INSERT INTO inventory (item_id, quantity, selling_price) VALUES (3, 26, 3000000);
INSERT INTO inventory (item_id, quantity, selling_price) VALUES (4, 47, 150000);

CREATE TABLE `transactions` (
	`id` int(11) NOT NULL,
	`item_id` int(11) NOT NULL,
	`quantity` int(11) NOT NULL,
	`price_sold` int(11) NOT NULL,
	`transaction_date` date NOT NULL,
	`employee` varchar(50) NOT NULL,
	KEY `item_id` (`item_id`),
	KEY `employee` (`employee`),
	CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
	CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`employee`) REFERENCES `users` (`username`)
);

CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `deliveries_view` AS select deliveries.id AS id, suppliers.name AS supplier, items_base.stock_num AS item, items_base.brand AS brand, items.description AS description, deliveries.quantity AS quantity, deliveries.buying_price AS buying_price, deliveries.delivery_date AS delivery_date from ((deliveries left join (items left join items_base on (items.item_base_id = items_base.id)) on (deliveries.item_id = items.id)) left join suppliers on(deliveries.supplier_id = suppliers.id));

CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `inventory_view` AS select inventory.item_id AS id, items_base.stock_num AS item, items_base.brand AS brand, items.description AS description, inventory.quantity AS quantity, inventory.selling_price AS selling_price from (inventory left join (items left join items_base on (items.item_base_id = items_base.id)) on (inventory.item_id = items.id));

CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW `transactions_view` AS select transactions.id AS id, items_base.stock_num AS item, items_base.brand AS brand, transactions.quantity AS quantity, transactions.price_sold AS price_sold, transactions.transaction_date AS transaction_date, concat(users.first_name, ' ', users.last_name) AS employee from ((transactions left join (items left join items_base on (items.item_base_id = items_base.id)) on((transactions.item_id = items.id))) left join users on((transactions.employee = users.username)));
